# shared
This repo is not for a specific project but a place that I can use to share some files with my friends and colleagues for brainstorming and knowledge sharing.

## Rules.
Please feel free to create new branch and merge requests but not merged into main.

### Loki

https://grafana.com/docs/loki/latest/setup/install/helm/install-scalable/

`helm repo add grafana https://grafana.github.io/helm-charts`

`helm install --values values.yaml loki grafana/loki`